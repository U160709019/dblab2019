#load data infile '/tmp/movie_stars.csv'
#into table movie_stars fields terminated by ',';
#show variables like 'secure_file_priv';

select Country, count(CustomerID)
from Customers
group by Country;

select * from company.Shippers;

select ShipperName, count(Orders.OrderID) as numberOfOrders
from Shippers join Orders on Shippers.ShipperID = Orders.ShipperID
group by ShipperName
order by numberOfOrders desc;

