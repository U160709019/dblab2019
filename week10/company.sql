select * from Customers;
-- delete Customers which have id = 20

delete from Customers where CustomerID=20;

-- costemers order bounded to ordersid so we first delete orders 

select * from Orders where CustomerID =20;
delete from Orders where CustomerID =20;
select * from Orders where CustomerID =20;

-- orders bounded to ordersdetaisl so we first delete details 
-- first we find orders and their own ids then we delete all details, and reverse the process

select * from OrderDetails where OrderID in(select OrderID from Orders where CustomerID =20);
DELETE from OrderDetails where OrderID in(select OrderID from Orders where CustomerID =20);

-- after delete all details, delete orders

select * from Orders where CustomerID =20;
delete from Orders where CustomerID =20;
select * from Orders where CustomerID =20;

-- after delete orders, delete customer

delete from Customers where CustomerID=20;



-- delete products which have id = 10

select * from Products;
DELETE from Products where ProductID =10;

select * from OrderDetails where ProductID=10;
DELETE from OrderDetails where ProductID =10;

DELETE from Products where ProductID =10;


-- delete shipper which have id = 2

select * from Shippers;

DELETE from Shippers where ShipperID =2;

select * from Orders where ShipperID =2;
delete from Orders where ShipperID =2;

select * from OrderDetails where OrderID in(select OrderID from Orders where ShipperID =2);
DELETE from OrderDetails where OrderID in(select OrderID from Orders where ShipperID =2);
select * from OrderDetails where OrderID in(select OrderID from Orders where ShipperID =2);

delete from Orders where ShipperID =2;
select * from Orders where ShipperID =2;

DELETE from Shippers where ShipperID =2;

select * from Shippers;

